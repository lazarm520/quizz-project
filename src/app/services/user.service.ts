import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class UserService {
url = environment.baseUrl;
  constructor(private http: HttpClient) {}
  register(data): Observable<any>  {
    return this.http.post('user/register', data);
  }
  login(data): Observable<any>  {
    return this.http.post('user/loginaction', data);
  }
}
