import { Component, OnInit, TemplateRef } from '@angular/core';
import { DownloadCsvService } from '../../services/download-csv.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import {UserService} from '../../services/user.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  jsonData = [
    {
      name: 'Anil Singh',
      age: 33,
      average: 98,
      approved: true,
      description: 'I am active blogger and Author.'
    },
    {
      name: 'Reena Singh',
      age: 28,
      average: 99,
      approved: true,
      description: 'I am active HR.'
    },
    {
      name: 'Aradhya',
      age: 4,
      average: 99,
      approved: true,
      description: 'I am engle.'
    },
  ]; accountClass = '';
  newClass = 'activModal';
  newsCustomer = false;
  modalRef2: BsModalRef;
  MselectSexe = 'selectedSexe';
  FselectSexe = 'unselectedSexe';
  signe = 'password';
  faLogin = 'fa-eye';
  mdp;
  votreEmail;
  formRegister: FormGroup;
  sexe = 'HOMME';
  constructor(
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private downService: DownloadCsvService,
    private userS: UserService) { this.createformRegister();}
  // tslint:disable-next-line: typedef
  getCSV() {
    this.downService.downloadFile(this.jsonData, 'jsontocsv');
  }
  // tslint:disable-next-line: typedef
  loginModal(template: TemplateRef<any>, a) {
    if (a.includes('compte')) {
      this.newsCustomer = false;
    } else {
      this.newsCustomer = true;
    }
    this.modalRef2 = this.modalService.show(
      template,
    );
  }
  newsCustomerBtn() {
    if (this.newsCustomer === false) {
      this.newsCustomer = true;
      this.newClass = '';
      this.accountClass = 'activModal';
    } else if (this.newsCustomer === true) {
      this.newsCustomer = false;
      this.newClass = 'activModal';
      this.accountClass = '';
    }
  }
  // tslint:disable-next-line: typedef
  createformRegister() {
    this.formRegister = this.formBuilder.group({
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      email: ['', Validators.compose([
        Validators.required,
        this.checkMail
      ])],
      password: ['', Validators.compose([
        Validators.required,
      ])],
    });
  }
  // tslint:disable-next-line: typedef
  checkMail(controls) {
    const regExp = new RegExp(/\S+@\S+\.\S+/);
    if (regExp.test(controls.value)) {
      return null;
    } else {
      return { checkMail: true };
    }
  }
  login(){
    const user = {
      mail: this.votreEmail,
      mdp: this.mdp
    };
    this.userS.login(user).subscribe((data:any) => {
      console.log(data)
    })
  }
  // tslint:disable-next-line: typedef
  createAccount() {}
  // tslint:disable-next-line: typedef
  selectSexe() {
    if (this.MselectSexe === 'selectedSexe' && this.FselectSexe === 'unselectedSexe') {
      this.FselectSexe = 'selectedSexe';
      this.MselectSexe = 'unselectedSexe';
      this.sexe = 'FEMME';
    } else if (this.FselectSexe === 'selectedSexe' && this.MselectSexe === 'unselectedSexe') {
      this.FselectSexe = 'unselectedSexe';
      this.MselectSexe = 'selectedSexe';
      this.sexe = 'HOMME';
    }
  }
  showPassLogin() {
    if (this.signe === 'password' && this.faLogin === 'fa-eye') {
      this.signe = 'text';
      this.faLogin = 'fa-eye-slash';
    } else {
      this.signe = 'password';
      this.faLogin = 'fa-eye';
    }
  }
  ngOnInit(): void {
  }

}
